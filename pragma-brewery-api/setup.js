const CacheClient = require('./src/infrastructure/cache/cache-client')
const cacheClient = new CacheClient()

const beerContainers = [ { "id": 1, "name": "Pilsner", "status": "Ok", "temperature": { "current": 4, "min": 4, "max": 6 } }, { "id": 2, "name": "IPA", "status": "Ok", "temperature": { "current": 6, "min": 5, "max": 6 } }, { "id": 3, "name": "Lager", "status": "Ok", "temperature": { "current": 4, "min": 4, "max": 7 } }, { "id": 4, "name": "Stout", "status": "Ok", "temperature": { "current": 7, "min": 6, "max": 8 } }, { "id": 5, "name": "Wheat beer", "status": "Ok", "temperature": { "current": 5, "min": 3, "max": 5 } }, { "id": 6, "name": "Pale Ale", "status": "Ok", "temperature": { "current": 4, "min": 4, "max": 6 } } ]

const setup = async () => {
  await cacheClient.set('truck:1', beerContainers)
  console.log('Setup executed with successfull!')
  process.exit(0)
}

setup()

