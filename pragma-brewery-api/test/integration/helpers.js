const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../src/core/app')
const CacheClient = require('../../src/infrastructure/cache/cache-client')

chai.use(chaiHttp)

global.request = chai.request(app).keepOpen();
global.cacheClient = new CacheClient()
global.expect = chai.expect;
