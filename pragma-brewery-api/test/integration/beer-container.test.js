const HttpStatus = require('http-status')
const beerContainers = require('./fixtures/beer-container')

describe('BeerContainer routes', () => {
  const TRUCK_ID = 1
  const BEER_CONTAINER_ID = 1

  beforeEach(async () => {
    await cacheClient.flushAll()
    cacheClient.set(`truck:${TRUCK_ID}`, beerContainers)
  })

  context(`GET /v1/trucks/${TRUCK_ID}/beer-containers`, () => {
    it(`should return a list of beer containers from truckId=${TRUCK_ID}`, async () => {
      const response = await request.get(`/v1/trucks/${TRUCK_ID}/beer-containers`)
      expect(response.status).to.be.eql(HttpStatus.OK)
      expect(response.body.beerContainers).to.be.eql(beerContainers)
    })
  })

  context(`GET /v1/trucks/10000/beer-containers`, () => {
    it(`should return an empty list of beer containers, truckId=10000 does not exist`, async () => {
      const response = await request.get(`/v1/trucks/10000/beer-containers`)
      expect(response.status).to.be.eql(HttpStatus.OK)
      expect(response.body.beerContainers).to.be.eql([])
    })
  })

  context(`GET /v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`, () => {
    it(`should return the beer container ${BEER_CONTAINER_ID}`, async () => {
      const response = await request.get(`/v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`)
      expect(response.status).to.be.eql(HttpStatus.OK)
      expect(response.body).to.be.eql(beerContainers[0])
    })
  })

  context(`GET /v1/trucks/10000/beer-containers/10000`, () => {
    it(`should return an empty object for beer container that does not exist`, async () => {
      const response = await request.get(`/v1/trucks/10000/beer-containers/10000`)
      expect(response.status).to.be.eql(HttpStatus.OK)
      expect(response.body).to.be.eql({})
    })
  })

  context(`UPDATE /v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`, () => {
    it(`should update the beerContainer=${BEER_CONTAINER_ID}`, async () => {
      const beerContainer = {
        "id": 1,
        "name": "Pilsner",
        "status": "Danger",
        "temperature": {
          "current": 7,
          "min": 4,
          "max": 6
        }
      }
      const response = await request.put(`/v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`).send(beerContainer)
      const beerContainerResponse = await request.get(`/v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`)
      expect(response.status).to.be.eql(HttpStatus.OK)
      expect(beerContainerResponse.body).to.be.eql(beerContainer)
    })
  })

  context(`DELETE /v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`, () => {
    it(`should delete the beerContainer=${BEER_CONTAINER_ID}`, async () => {
      const response = await request.delete(`/v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`)
      const beerContainerResponse = await request.get(`/v1/trucks/${TRUCK_ID}/beer-containers/${BEER_CONTAINER_ID}`)
      expect(response.status).to.be.eql(HttpStatus.OK)
      expect(beerContainerResponse.body).to.be.eql({})
    })
  })
})
