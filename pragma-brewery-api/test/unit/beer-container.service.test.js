const _ = require('lodash')
const expectedBeerContainers = require('./fixtures/beer-containers.json')
const CacheClientFake = require('./fakes/cache-client.fake')
const WebSocketFake = require('./fakes/websocket.fake')
const BeerContainerRepository = require('../../src/domain/beer-containers/beer-container.repository')
const BeerContainerService = require('../../src/domain/beer-containers/beer-container.service')

const TRUCK_ID = 1
const BEER_CONTAINER_ID = 1

describe('BeerContainerService', () => {
  let beerContainerRepository
  let beerContainerService
  let updateSpy
  let removeSpy
  let emitSpy

  beforeEach(() => {
    beerContainerRepository = new BeerContainerRepository(CacheClientFake)
    beerContainerService = new BeerContainerService(beerContainerRepository, WebSocketFake)
    emitSpy = sinon.spy(WebSocketFake, 'emit')
    updateSpy = sinon.spy(beerContainerRepository, 'update')
    removeSpy = sinon.spy(beerContainerRepository, 'remove')
  })

  afterEach(() => {
    updateSpy.restore()
    removeSpy.restore()
    emitSpy.restore()
  })

  context('findAll()', () => {
    it('should return a list of beer containers', async () => {
      const beerContainers = await beerContainerService.findAll(TRUCK_ID)
      expect(beerContainers).to.be.eql(expectedBeerContainers)
    })
  })

  context('findById()', () => {
    it(`should return the beer container ${BEER_CONTAINER_ID}`, async () => {
      const beerContainer = await beerContainerService.findById(TRUCK_ID, BEER_CONTAINER_ID)
      const expectedBeerContainer = _.find(expectedBeerContainers, ['id', BEER_CONTAINER_ID])
      expect(beerContainer).to.be.eql(expectedBeerContainer)
    })
  })

  context('update()', () => {
    it(`should update the beer container ${BEER_CONTAINER_ID}`, async() => {
      const beerContainer = {
        id: 1,
        name: 'Pilsner',
        status: 'Danger',
        temperature: {
          current: 7,
          min: 4,
          max: 6
        }
      }
      await beerContainerService.update(TRUCK_ID, BEER_CONTAINER_ID, beerContainer)
      sinon.assert.calledOnce(updateSpy);
      sinon.assert.calledWith(updateSpy, TRUCK_ID, BEER_CONTAINER_ID, beerContainer);
      sinon.assert.calledOnce(emitSpy)
    })
  })

  context('remove()', () => {
    it(`should remove the beer container ${BEER_CONTAINER_ID}`, async() => {
      await beerContainerService.remove(TRUCK_ID, BEER_CONTAINER_ID)
      sinon.assert.calledOnce(removeSpy);
      sinon.assert.calledWith(removeSpy, TRUCK_ID, BEER_CONTAINER_ID);
    })
  })
})
