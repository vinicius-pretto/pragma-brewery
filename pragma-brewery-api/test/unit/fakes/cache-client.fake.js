const beerContainers = require('../fixtures/beer-containers.json')

class CacheClientFake {
  static async get(truckId) {
    return beerContainers
  }

  static async set(key, value) {
    
  }
}

module.exports = CacheClientFake

