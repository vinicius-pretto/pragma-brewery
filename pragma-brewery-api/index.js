const app = require('./src/core/app');
const config = require('./config');

const bootstrap = async () => {
  await app.listen(config.port);
  console.log(`PragmaBrewery API is listening at port ${config.port}`);
};

bootstrap();
