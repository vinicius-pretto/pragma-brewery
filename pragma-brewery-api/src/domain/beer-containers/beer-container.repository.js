const _ = require('lodash')

class BeerContainerRepository {
  constructor(CacheClient) {
    this.cacheClient = CacheClient
  }

  buildKey(truckId) {
    return `truck:${truckId}`
  }

  findAll(truckId) {
    return this.cacheClient.get(this.buildKey(truckId))
  }

  async findById(truckId, beerContainerId) {
    const beerContainers = await this.findAll(truckId)
    return _.find(beerContainers, ['id', Number(beerContainerId)])
  }

  async update(truckId, beerContainerId, beerContainer) {
    const beerContainers = await this.findAll(truckId)
    const index = _.findIndex(beerContainers, ['id', Number(beerContainerId)])
    const hasBeerContainer = index !== -1
    if (hasBeerContainer) {
      beerContainers[index] = beerContainer
      return this.cacheClient.set(this.buildKey(truckId), beerContainers)
    }
    return Promise.resolve()
  }

  async remove(truckId, beerContainerId) {
    const beerContainers = await this.findAll(truckId)
    _.remove(beerContainers, ['id', Number(beerContainerId)])
    return this.cacheClient.set(this.buildKey(truckId), beerContainers)
  }
}

module.exports = BeerContainerRepository
