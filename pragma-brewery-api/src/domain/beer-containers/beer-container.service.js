const SocketEvent = require('../../core/socket-event.enum')
const { isEmpty } = require('lodash')

class BeerContainerService {
  constructor(BeerContainerRepository, webSocket) {
    this.beerContainerRepository = BeerContainerRepository
    this.webSocket = webSocket
  }

  async findAll(truckId) {
    const beerContainers = await this.beerContainerRepository.findAll(truckId)
    return isEmpty(beerContainers) ? [] : beerContainers
  }

  async findById(truckId, beerContainerId) {
    const beerContainer = await this.beerContainerRepository.findById(truckId, beerContainerId)
    return beerContainer || {}
  }

  async update(truckId, beerContainerId, beerContainer) {
    await this.beerContainerRepository.update(truckId, beerContainerId, beerContainer)
    const beerContainers = await this.findAll(truckId)
    this.webSocket.emit(SocketEvent.BEER_CONTAINER_UPDATE, beerContainers)
  }

  async remove(truckId, beerContainerId) {
    await this.beerContainerRepository.remove(truckId, beerContainerId)
    const beerContainers = await this.findAll(truckId)
    this.webSocket.emit(SocketEvent.BEER_CONTAINER_UPDATE, beerContainers)
  }
}

module.exports = BeerContainerService

