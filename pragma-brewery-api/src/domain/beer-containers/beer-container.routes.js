const HttpStatus = require('http-status')
const CacheClient = require('../../infrastructure/cache/cache-client')
const BeerContainerRepository = require('./beer-container.repository')
const BeerContainerService = require('./beer-container.service')

module.exports = (app, webSocket) => {
  const cacheClient = new CacheClient()
  const beerContainerRepository = new BeerContainerRepository(cacheClient)
  const beerContainerService = new BeerContainerService(beerContainerRepository, webSocket)

  app.get('/v1/trucks/:truckId/beer-containers', async (req, res) => {
    const beerContainers = await beerContainerService.findAll(req.params.truckId)
    res.json({ beerContainers: beerContainers })
  })

  app.get('/v1/trucks/:truckId/beer-containers/:beerContainerId', async (req, res) => {
    const beerContainer = await beerContainerService.findById(req.params.truckId, req.params.beerContainerId)
    res.json(beerContainer)
  })

  app.put('/v1/trucks/:truckId/beer-containers/:beerContainerId', (req, res) => {
    beerContainerService.update(req.params.truckId, req.params.beerContainerId, req.body)
    res.sendStatus(HttpStatus.OK)
  })

  app.delete('/v1/trucks/:truckId/beer-containers/:beerContainerId', (req, res) => {
    beerContainerService.remove(req.params.truckId, req.params.beerContainerId)
    res.sendStatus(HttpStatus.OK)
  })
}
