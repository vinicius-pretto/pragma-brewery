const express = require('express')
const bodyParser = require('body-parser')
const config = require('../../config')
const app = express();
const httpServer = require('http').Server(app)
const webSocketServer = require('socket.io')(httpServer)
const SocketEvent = require('./socket-event.enum')

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', config.pragmaBreweryUrl)
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

app.use(bodyParser.json())

require('./router')(app, webSocketServer)

webSocketServer.on(SocketEvent.CONNECTION, socket => {
  console.log('a user connected', socket.id)
})

module.exports = httpServer
