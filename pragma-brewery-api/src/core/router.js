module.exports = (app, webSocketServer) => {
  require('../domain/beer-containers/beer-container.routes')(app, webSocketServer)
}
