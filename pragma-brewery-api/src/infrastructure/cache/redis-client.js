const Promise = require('bluebird');
const config = require('../../../config');
const redis = require('redis');
const client = redis.createClient([config.redis]);

client.on('connect', () => {
  console.log('Redis: connection successful!');
});

client.on('error', (error) => {
  console.log('Redis: connection failed!');
  console.log(error);
});

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

module.exports = client;
