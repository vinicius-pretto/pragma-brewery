const redisClient = require('./redis-client')

class CacheClient {
  toJSON(data) {
    return data ? JSON.parse(data) : {};
  }

  get(key) {
    return redisClient
      .getAsync(key)
      .then(this.toJSON)
  }

  set(key, value) {
    return redisClient.setAsync(String(key), JSON.stringify(value));
  }

  flushAll() {
    return redisClient.flushallAsync()
  }
}

module.exports = CacheClient
