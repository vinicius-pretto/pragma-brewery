const config = {
  port: process.env.PORT || 8080,
  pragmaBreweryUrl: process.env.PRAGMA_BREWERY_URL || 'http://localhost:3000',
  // redis
  redis: {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: process.env.REDIS_PORT || 6379,
    password: process.env.REDIS_PASSWORD || ''
  }
};

module.exports = config;
