export const SocketEvent = Object.freeze({
  CONNECTION: 'connection',
  // container beers
  BEER_CONTAINER_UPDATE: 'beer_container.update'
})
