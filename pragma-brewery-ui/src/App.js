import React, { Component } from 'react';
import './App.css';
import { BeerContainers } from './components/BeerContainers'

class App extends Component {
  render() {
    return (
      <BeerContainers />
    )
  }
}

export default App;
