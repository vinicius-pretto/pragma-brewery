import React from 'react'
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const style = {
  text: {
    color: '#FFF'
  },
  defaultStatus: {
    background: '#333'
  },
  dangerStatus: {
    background: '#F44336'
  }
}

export const BeerContainer = ({ status, name, temperature }) => {
  const containerStyle = status === 'Danger' ? style.dangerStatus : style.defaultStatus
  return (
    <Card style={containerStyle}>
      <CardHeader
        title={
          <Typography variant='title' style={style.text}>
            {name} Container
          </Typography>
        }
        subheader={
          <Typography style={style.text}>
            Min Temperature: {temperature.min}°C <br/>
            Max Temperature: {temperature.max}°C
          </Typography>
        }
      />
      <CardContent>
        <Typography align='right' variant='title' style={style.text}>
          {temperature.current}°C
        </Typography>
      </CardContent>
    </Card>
  )
}
