import React, { Component } from 'react'
import { BeerContainerService } from './BeerContainerService'
import { BeerContainer } from './BeerContainer'
import { SocketEvent } from '../../SocketEvent'
import Grid from '@material-ui/core/Grid'
import webSocketClient from 'socket.io-client'

export class BeerContainers extends Component {
  constructor() {
    super()
    this.webSocket = webSocketClient.connect(process.env.PRAGMA_API_URL)
    this.beerContainerService = new BeerContainerService()
    this.state = {
      beerContainers: []
    }
  }

  async componentDidMount() {
    const beerContainers = await this.beerContainerService.findBeerContainers()
    this.setState({ beerContainers: beerContainers })
    this.webSocket.on(SocketEvent.BEER_CONTAINER_UPDATE, beerContainersUpdated => {
      this.setState({ beerContainers: beerContainersUpdated })
    })
  }

  render() {
    return (
      <Grid container spacing={24} style={{ padding: '20px' }}>
        {this.state.beerContainers.map((beer) => 
          <Grid key={beer.id} item xs={12} sm={6} md={4}>
            <BeerContainer {...beer} />
          </Grid>
        )}
      </Grid>
    );
  }
}
