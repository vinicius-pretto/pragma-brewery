export class BeerContainerService {
  async findBeerContainers() {
    try {
      const response = await fetch(`${process.env.PRAGMA_API_URL}/v1/trucks/1/beer-containers`)
      const jsonResponse = await response.json()
      return jsonResponse.beerContainers
    } catch (error) {
      console.error('Error on fetch beers container', error);
    }
  }
}
