# PragmaBrewery - Code Challenge

## Requirements

* [Node.js >= 8.11.3](https://nodejs.org/en/)
* [Redis](https://redis.io/) or [Docker](https://www.docker.com/)

## Running the application

### Clone the repository

```
$ git clone https://vinicius-pretto@bitbucket.org/vinicius-pretto/pragma-brewery.git
```

### Running a Redis

* Option 1 - Download and run Redis Server with default settings
* Option 2 - Start a Docker container

```
$ ./scripts/setup.sh
```

### Start API

```
$ cd pragma-brewery-api
$ npm i 
$ npm run setup
$ npm start
```

### Start UI

```
$ cd pragma-brewery-ui
$ npm i 
$ npm start
```

## Running the tests

```
$ cd pragma-brewery-api
$ npm t
```

## Simulate the temperatures

```
$ cd thermometer-mock
$ npm i 
$ npm start
```

## Assumptions

* Each container has only one type of a beer
* There is a way to input the temperature by calling a REST endpoint
* Possibility to have a local server in the truck or internet connection
* There is a tablet in the truck

## Highlights

* Automation tests
* Websockets
* CI

## Version 2.0

* Add a previous screen to make it possible to monitor other trucks, if necessary
* Add tests in client side
* Add E2E tests
* Add a linter
* Improve error handling 
* Add validation in the endpoints
* Add logs
