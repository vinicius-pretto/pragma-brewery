const axios = require('axios')
const PRAGMA_API_URL = 'http://localhost:8080'
let timeout = 1000

const beerContainers = {
  'Pilsner': {
    id: 1,
    name: 'Pilsner',
    status: 'Ok',
    temperature: {
      current: 4,
      min: 4,
      max: 6
    }
  },
  'IPA': {
    id: 2,
    name: 'IPA',
    status: 'Ok',
    temperature: {
      current: 6,
      min: 5,
      max: 6
    }
  },
  'Lager': {
    id: 3,
    name: 'Lager',
    status: 'Ok',
    temperature: {
      current: 4,
      min: 4,
      max: 7
    }
  },
  'Stout': {
    id: 4,
    name: 'Stout',
    status: 'Ok',
    temperature: {
      current: 7,
      min: 6,
      max: 8
    }
  },
  'Wheat beer': {
    id: 5,
    name: 'Wheat beer',
    status: 'Danger',
    temperature: {
      current: 5,
      min: 3,
      max: 5
    }
  },
  'Pale Ale': {
    id: 6,
    name: 'Pale Ale',
    status: 'Ok',
    temperature: {
      current: 4,
      min: 4,
      max: 6
    }
  }
}

const update = (containerName, temperature) => {
  const beerContainer = {...beerContainers[containerName]}
  beerContainer.temperature.current = temperature

  if (temperature < beerContainer.temperature.min || temperature > beerContainer.temperature.max) {
    beerContainer.status = 'Danger'
  } else {
    beerContainer.status = 'Ok'
  }
  console.log(`Update ${beerContainer.name} container temperature=${beerContainer.temperature.current} status=${beerContainer.status}`)
  return axios.put(`${PRAGMA_API_URL}/v1/trucks/1/beer-containers/${beerContainer.id}`, beerContainer)
}

emitUpdate = (status, temperature) => {
  timeout += 1000

  setTimeout(() => {
    update(status, temperature)
  }, timeout)
}

emitUpdate('Pilsner', 1)
emitUpdate('Pale Ale', 3)
emitUpdate('Wheat beer', 2)
emitUpdate('Pilsner', 10)
emitUpdate('Stout', 5)
emitUpdate('IPA', 9)
emitUpdate('Lager', 6)
emitUpdate('Wheat beer', 5)
emitUpdate('Pilsner', 5)
emitUpdate('Stout', 5)
emitUpdate('IPA', 6)
emitUpdate('Pale Ale', 4)
emitUpdate('Wheat beer', 5)
emitUpdate('Pilsner', 4)
emitUpdate('Stout', 5)
emitUpdate('IPA', 10)

